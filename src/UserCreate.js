import React, { useState } from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';



const theme = createTheme();

export default function UserCreate() {

  const handleSubmit = event => {
    event.preventDefault();
    const data = {
      'username2': name,
      'licenseplate': licenseplate,
      'date': date,
      'time': time
    }
    fetch("http://localhost:3333/create", {
      method: 'POST',
      headers: {
        Accept: 'application/from-data',
        'Content-type': 'application/json'
      },
      body: JSON.stringify(data)
      })
      .then(res => res.json())
      .then(
        (result) => {
          if(result['status'] === 'ok'){
            window.location.href = '/inf'
          }
        },
      )
  }

  const [name, setName] = useState('');
  const [licenseplate, setLicenseplate] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          
          <Typography component="h1" variant="h5">
            Create Users
          </Typography>
          <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  autoComplete="username2"
                  name="firstName"
                  variant="outlined"
                  required
                  fullWidth
                  id="username2"
                  label="Name"
                  onChange={(e) => setName(e.target.value)}
                  autoFocus
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="licenseplate"
                  label="Licenseplate"
                  name="licenseplate"
                  onChange={(e) => setLicenseplate(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="date"
                  label="Date"
                  type="date"
                  name="date"
                  onChange={(e) => setDate(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="time"
                  label="Time"
                  type="time"
                  id="time"
                  onChange={(e) => setTime(e.target.value)}
                />
              </Grid>
              
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Submit
            </Button>
          
          </Box>
        </Box>
        
      </Container>
    </ThemeProvider>
  );
}