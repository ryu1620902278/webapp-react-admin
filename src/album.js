import React, {useEffect} from 'react';
import { useState } from 'react'
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import CameraIcon from '@mui/icons-material/PhotoCamera';

import CssBaseline from '@mui/material/CssBaseline';

import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';


import { createTheme, ThemeProvider } from '@mui/material/styles';

import Axios from 'axios'


function Copyright() {
    return (
    <Typography variant="body2" color="text.secondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://mui.com/">
        Your Website
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
    </Typography>
    );

    
}

const theme = createTheme();

export default function Album() {
    useEffect(() => {
        const token = localStorage.getItem('token') 
        fetch('http://localhost:3333/authen', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer '+token
  },
  
})
.then(response => response.json())
.then(data => {
    if(data.status === 'ok') {
        alert('authen success');
    }else{
        alert('authen failed')
        localStorage.removeItem('token');
        window.location = '/login'
    }
  console.log('Success:', data);
})
.catch((error) => {
  console.error('Error:', error);
});
    }, [])
    
    
    const handleLogout = (event) => {
        event.preventDefault();
        localStorage.removeItem('token');
        window.location = '/login'
    }
    const handleReg = (event) => {
        event.preventDefault();
        localStorage.removeItem('token');
        window.location = '/App'
    }

    const [customerList, setCustomerList] = useState([]);

    const getCustomer = () => {
        Axios.get('http://localhost:3333/user3').then((response) => {
        setCustomerList(response.data);
        
        })
        
    }

return (
    <ThemeProvider theme={theme}>
        <CssBaseline />
        <AppBar position="relative">
            <Toolbar>
        <CameraIcon sx={{ mr: 2 }} />
        <Typography variant="h5" color="inherit" noWrap>
            Customer Information
        
            
            
        </Typography>
        </Toolbar>
        </AppBar>
        <main>
        {/* Hero unit */}
        
        <Container maxWidth="sm">
            
            <Stack
                sx={{ pt: 4 }}
                direction="row"
                spacing={2}
                justifyContent="center"
            >
                
                
                <Button variant="contained" onClick={handleReg}>Add Customer Information </Button>
            </Stack>
            </Container>
        

        <div className="Customer">
        <Stack
                sx={{ pt: 4 }}
                direction="row"
                spacing={2}
                justifyContent="center"
            >
                
                <Button variant ="contained" onClick={getCustomer}>Show Information Customer</Button>
                <button className="btn btn-danger" onClick={handleLogout}>Logout</button>
            </Stack>
      
      <br/>
      {customerList.map((val, key) => {
        return (
          
              
              <table border="1" cellpadding="7" cellspacing="7" align="center">
              <tr>
                
                <th width="200px">Name</th>
                <th width="250px">License Plate</th>
                <th width="200px">Date</th>
                <th width="200px">Time</th>
              </tr>
              <tr>
                <td className="cell">{val.name}</td>
                <td className="cell">{val.licenseplate}</td>
                <td className="cell">{val.date}</td>
                <td className="cell">{val.time}</td>
  
                </tr>
              </table>
              
            //</div>
          //</div>
          
        )
    })}
    </div>

        
        
    </main>
      {/* Footer */}
    <Box sx={{ bgcolor: 'background.paper', p: 6 }} component="footer">
        <Typography variant="h6" align="center" gutterBottom>
            Footer
        </Typography>
        <Typography
            variant="subtitle1"
            align="center"
            color="text.secondary"
            component="p"
        >
            Something here to give the footer a purpose!
        </Typography>
        <Copyright />
    </Box>
      {/* End footer */}
    </ThemeProvider>
);
}
