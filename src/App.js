import Axios from 'axios'
import { useState } from 'react'

function App() {

  const [name,setName] = useState("");
  const [licenseplate,setLicenseplate] = useState("");

  const [custList, setCustList] = useState([]);

  

  const addUser = () => {
    Axios.post('http://localhost:3333/map', {
      name: name,
      licenseplate: licenseplate
    }).then(() => {
      setCustList([
        ...custList,
      { name: name,
        licenseplate: licenseplate
      }
      ])
    })
  }
  const handleLogout = (event) => {
    event.preventDefault();
    localStorage.removeItem('token');
    window.location = '/inf'
}

  return (
    <div className="App container">
      <h1>Add Customer Information</h1>
      <div className="information">
        <form action="">
          <div className="mb-3">
            <label htmlFor="name" className="form-label">
              Name:
            </label>
            <input
              type="text"
              className="form-control"
              placeholder="Enter name"
              onChange={(event) => {
                setName(event.target.value)
              }}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="name" className="form-label">
              License Plate:
            </label>
            <input
              type="text"
              className="form-control"
              placeholder="Enter license plate"
              onChange={(event) => {
                setLicenseplate(event.target.value)
              }}
            />
          </div>
          <button className="btn btn-success" onClick={addUser}>Submit</button>
          <br></br>
          <br></br>
          <button className="btn btn-danger" onClick={handleLogout}>Return</button>

          </form>
      </div>
    </div>
  )
}

export default App;