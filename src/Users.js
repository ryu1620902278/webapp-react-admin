import React, { useState, useEffect } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Link from '@mui/material/Link';
import ButtonGroup from '@mui/material/ButtonGroup';
import Axios from 'axios';
import Avatar from '@mui/material/Avatar';




export default function Users() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch("http://localhost:3333/user3")
      .then(res => res.json())
      .then(
        (result) => {
          setItems(result);
        }
      )
  }, [])
  const handleReg = (event) => {
    event.preventDefault();
    localStorage.removeItem('token');
    window.location = '/App'
}

  

  const UserDelete = id => {
    Axios.delete(`http://localhost:3333/del/${id}`).then((response) => {
      setItems(
        items.filter((row) => {
          return row.id !== id;
        })
      )
    })
   }
  const UserUpdate = id => {
  window.location = '/Ine/'+ id  
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg" sx={{ p: 2}}>
        <Paper sx= {{p:2}}>
            <Box display = "flex">
                <Box sx={{ flexGrow: 1 }}>
                    <Typography variant="h6" gutterBottom component="div">
                        Table Information
                    </Typography></Box>
                <Box>
                    <Link href="edit">
                     <Button variant="contained"onClick={handleReg}>Add Information</Button>
                     
                    </Link>    
                </Box>        
            </Box>
            <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="center" >Picture</TableCell>
                            <TableCell align="center" >Name</TableCell>
                            <TableCell align="center">License Plate</TableCell>
                            <TableCell align="center">Date</TableCell>
                            <TableCell align="center">Time</TableCell>
                            
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {items.map((row) => (
                            <TableRow
                            key={row.name}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                            <TableCell component="th" scope="row">
                                {row.id}
                            </TableCell>
                            <TableCell align="center">
                              <Box>
                                <Avatar sx={{ width: 100, height: 100 }} src={row.picture} variant = "square" />
                                
                              </Box>
                            </TableCell>
                            <TableCell align="center">{row.name}</TableCell>
                            <TableCell align="center">{row.licenseplate}</TableCell>
                            <TableCell align="center">{row.date}</TableCell>
                            <TableCell align="center">{row.time}</TableCell>
                            <TableCell align="center">
                                <ButtonGroup variant="outlined" aria-label="outlined button group">
                                    <Button onClick={() => { UserUpdate(row.id)}}>Edit</Button>
                                    <Button onClick={() => { UserDelete(row.id)}}>Del</Button>
                                    
                                </ButtonGroup>
                            </TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
                    </Table>
            </TableContainer>
        </Paper>
       </Container>
    </React.Fragment>
  );
}  
     