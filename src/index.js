import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route} from "react-router-dom";
import Login from './login.js'
import Album from './album.js'
import Register from './register'
import App from './App'
import Users from './Users'
import Inf from './Inf'
import UserEdit from './UserEdit'
import UserUpdate from './UserUpdate'
import UserCreate from './UserCreate'
import Ind from './Ind'
import Ine from './Ine'


ReactDOM.render(
  <BrowserRouter>
    
    <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/login" element={<Login />} />
      <Route path="/album" element={<Album />} />
      <Route path="/register" element={<Register />} />
      <Route path="/App" element={<App/>} />
      <Route path="/Users" element={<Users />} />
      <Route path="/Inf" element={<Inf />} />
      <Route path="/UserEdit" element={<UserEdit />} />
      <Route path="/UserUpdate/:id" element={<UserUpdate />} />
      <Route path="/UserCreate" element={<UserCreate />} />
      <Route path="/Ind" element={<Ind />} />
      <Route path="/Ine/:id" element={<Ine />} />
      
      
    </Routes>
    
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();